<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.index');
// });
// Auth::routes();
Route::get('/', 'HomeController@home')->name('login');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/logoutKeyloack', 'HomeController@logout')->name('logoutKeyloack');
Route::get('/loginKeycloack', 'HomeController@login')->name('loginKeycloack');
Route::get('/updateKeycloack', 'HomeController@update')->name('updateKeycloack');
Route::post('/update_profil', 'HomeController@update_profil')->name('update_profil');
Route::post('/change_password', 'HomeController@change_password')->name('change_password');
Route::get('/changepassword', 'HomeController@changepassword')->name('changepassword');

Route::get('/register', 'RegisterController@index')->name('register');
Route::post('/register/store', 'RegisterController@store')->name('register.store');
Route::get('/verify/{id}', 'RegisterController@verify')->name('verify');
Route::post('/verify-get/{id}', 'RegisterController@verify-get')->name('verify.get');

Route::get('/showUserRole', 'HomeController@showUserRole')->name('showUserRole');
Route::get('/showUserRole/{id}', 'HomeController@showUserRoleID')->name('showUserRoleID');
Route::post('/userRoleIDUpdate', 'HomeController@userRoleIDUpdate')->name('userRoleIDUpdate');

Route::get('/logout', 'RegisterController@logout')->name('logout');
