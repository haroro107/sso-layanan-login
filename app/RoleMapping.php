<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMapping extends Model
{
    protected $fillable = [
        'client_id', 'client_name'
    ];
}
