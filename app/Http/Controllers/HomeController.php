<?php

namespace App\Http\Controllers;

use App\Role;
use App\RoleMapping;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;


class HomeController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function home()
    {
        return view('auth.index');
    }
    public function index()
    {
        $user = Auth::user();
        $role_mappings = RoleMapping::where('user_id', $user->id)->get();

        return view('home', compact('user', 'role_mappings'));
    }

    public function login(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->post(env('KEYCLOAK_SERVER_URI') . 'auth/realms/SPBE/protocol/openid-connect/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'code' => $request->code,
                'client_id' => env('KEYCLOAK_SERVER_ID'),
                'client_secret' => env('KEYCLOAK_SERVER_SECRET'),
                'redirect_uri' => env('KEYCLOAK_SERVER_REDIRECT_URI'),
            ]
        ]);

        $json = json_decode((string) $response->getBody(), true);
        // dd($r->getBody);

        $user = $client->get(env('KEYCLOAK_SERVER_URI') . 'auth/realms/SPBE/protocol/openid-connect/userinfo', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $json['access_token'],
            ],
        ]);

        $data = json_decode((string) $user->getBody(), true);

        $roles = 'Tidak Ada Role';

        foreach ($data['resource_access'] as $key => $value) {
            if ($key == env('KEYCLOAK_SERVER_ID')) {
                $roles = $value['roles'][0];
            }
        }

        $getToken = $this->getToken();

        $username = $data['preferred_username'];
        $email = $data['email'];
        $firstname = $data['given_name'];
        $lastname = $data['family_name'];
        $user_secret = $data['sub'];
        $token = $json['access_token'];

        $user = User::where('username', $username)->get()->first();
        if ($user) {
            $user->username = $username;
            $user->email = $email;
            $user->firstname = $firstname;
            $user->lastname = $lastname;
            $user->password = 123;
            $user->user_secret = $user_secret;
            $user->roles = $roles;
            $user->save();

            RoleMapping::whereIn('client_id', [$user->id])->delete();
            RoleMapping::where('user_id', $user->id)->delete();

            $role_mappings = $client->get(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users/' . $data['sub'] . '/role-mappings', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $getToken,
                ],
            ]);

            $role_mappings = json_decode((string) $role_mappings->getBody(), true);

            foreach ($role_mappings['clientMappings'] as $key => $value) {
                $client_data = $client->get(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/clients', [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer ' . $getToken,
                    ],
                ]);

                $client_data = json_decode((string) $client_data->getBody(), true);


                foreach ($client_data as $key2 => $item) {
                    if ($item['clientId'] == $value['client'] && $value['client'] != 'account' && $value['client'] != 'aplikasi-testing' &&  $value['client'] != 'realm-management') {
                        $role_mappings = new RoleMapping;

                        $newstr = str_replace("*", "", $item['redirectUris'][0]);

                        $role_mappings->user_id = $user->id;
                        $role_mappings->client_id = $value['id'];
                        $role_mappings->client_name = $value['client'];
                        $role_mappings->redirectUris = $newstr;

                        $role_mappings->save();
                    }
                }
            }

            Auth::loginUsingId($user->id);
            return redirect()->route('home');
        } else {

            $user = new User;

            $user->username = $username;
            $user->email = $email;
            $user->firstname = $firstname;
            $user->lastname = $lastname;
            $user->user_secret = $user_secret;
            $user->password = 123;
            $user->role_id = 2;
            $user->roles = $roles;

            $user->save();
            RoleMapping::whereIn('client_id', [$user->id])->delete();
            RoleMapping::where('user_id', $user->id)->delete();

            $role_mappings = $client->get(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users/' . $data['sub'] . '/role-mappings', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $getToken,
                ],
            ]);

            $role_mappings = json_decode((string) $role_mappings->getBody(), true);

            foreach ($role_mappings['clientMappings'] as $key => $value) {
                $client_data = $client->get(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/clients', [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer ' . $getToken,
                    ],
                ]);

                $client_data = json_decode((string) $client_data->getBody(), true);

                foreach ($client_data as $key2 => $item) {
                    if ($item['clientId'] == $value['client'] && $value['client'] != 'account' && $value['client'] != 'aplikasi-testing' && $value['client'] != 'realm-management') {
                        $role_mappings = new RoleMapping;

                        $newstr = str_replace("*", "", $item['redirectUris'][0]);

                        $role_mappings->user_id = $user->id;
                        $role_mappings->client_id = $value['id'];
                        $role_mappings->client_name = $value['client'];
                        $role_mappings->redirectUris = $newstr;

                        $role_mappings->save();
                    }
                }
            }


            Auth::loginUsingId($user->id);
            return redirect()->route('home');
        }
    }

    public function update()
    {
        $user = Auth::user();
        $role = Role::all();

        return view('profil.update', compact('user', 'role'));
    }

    public function update_profil(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->email = $request->email;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        // $user->role_id = $request->role_id;
        $user->save();

        $client = new \GuzzleHttp\Client();

        $array = [
            'email' => $request->email,
            'firstName' => $request->firstname,
            'lastName' => $request->lastname
        ];

        $request = $client->put(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users/' . $user->user_secret, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->getToken(),
            ],
            'body' => json_encode($array),
        ]);

        return redirect()->route('home');
    }


    public function changepassword()
    {
        return view('profil.changepassword');
    }

    public function change_password(Request $request)
    {
        $user = Auth::user();
        $role = Role::all();

        $client = new \GuzzleHttp\Client();

        $array = ['type' => 'password', 'value' => $request->password1, 'temporary' => false];

        $request = $client->put(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users/' . $user->user_secret . '/reset-password', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->getToken(),
            ],
            'body' => json_encode($array),
        ]);


        return redirect()->route('home');
    }

    public function logout()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users/' . auth()->user()->user_secret . '/logout', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->getToken(),
        ],]);

        Auth::logout();
        Session::flush();

        return view('auth.index');
    }



    public function showUserRole()
    {
        $user = User::all();
        return view('profil.showUserRole', compact('user'));
    }
    public function showUserRoleID($id)
    {
        $user = User::findorfail($id);
        $client = new \GuzzleHttp\Client();
        $getToken = $this->getToken();
        $client_data = $client->get(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/clients', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $getToken,
            ],
        ]);

        $client_data = json_decode((string) $client_data->getBody(), true);

        foreach ($client_data as $key2 => $item) {
            if ($item['clientId'] == env('KEYCLOAK_SERVER_ID')) {
                $role_data = $client->get(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/clients/' . $item['id'] . '/roles', [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer ' . $getToken,
                    ],
                ]);

                $role_data = json_decode((string) $role_data->getBody(), true);
            }
        }
        return view('profil.showUserRoleID', compact('role_data', 'user'));
    }
    public function userRoleIDUpdate(Request $request)
    {
        $user = Auth::user();
        $client = new \GuzzleHttp\Client();
        $result_role = explode('|', $request->role_id);
        $token = $this->getToken();

        $respone = $client->get(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users/' . $user->user_secret . '/role-mappings/clients/' . $result_role[2], [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
        ]);

        $data_user_role = json_decode((string) $respone->getBody(), true);

        foreach ($data_user_role as $key => $value) {
            $array = [[
                'id' => $value['id'],
                "name" => $value['name'],
                "composite" => false,
                "clientRole" => true,
                "containerId" => $value['containerId']
            ]];
            $respone = $client->delete(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users/' . $user->user_secret . '/role-mappings/clients/' . $result_role[2], [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
            ]);
        }

        $array = [[
            'id' => $result_role[0],
            "name" => $result_role[1],
            "composite" => false,
            "clientRole" => true,
            "containerId" => $result_role[2]
        ]];

        $respone = $client->post(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users/' . $user->user_secret . '/role-mappings/clients/' . $result_role[2], [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
            'body' => json_encode($array),
        ]);
        return redirect()->route('home');
    }

    public function getToken()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('KEYCLOAK_SERVER_URI') . 'auth/realms/SPBE/protocol/openid-connect/token', [
            'form_params' => [
                'client_id' => env('KEYCLOAK_SERVER_ID'),
                'client_secret' => env('KEYCLOAK_SERVER_SECRET'),
                'grant_type' => 'client_credentials'
            ]
        ]);

        $access_token = json_decode((string) $response->getBody(), true);

        return $access_token['access_token'];
    }

    public function getUserData()
    {
        $client = new \GuzzleHttp\Client();
        $getToken = $this->getToken();
        $client_data = $client->get(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $getToken,
            ],
        ]);

        $client_data = json_decode((string) $client_data->getBody(), true);

        foreach ($client_data as $key2 => $item) {
            if ($item['clientId'] == env('KEYCLOAK_SERVER_ID')) {
                $role_data = $client->get(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users/' . $item['id'] . '/roles', [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer ' . $getToken,
                    ],
                ]);

                $role_data = json_decode((string) $role_data->getBody(), true);
            }
        }
    }
}
