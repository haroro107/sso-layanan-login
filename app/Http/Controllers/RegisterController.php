<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index()
    {
        return view('auth.register');
    }
    public function store(Request $request)
    {
        $client = new \GuzzleHttp\Client();

        // $array = [
        //     'enabled' => true,
        //     'email' => $request->email,
        //     'username' => $request->username,
        //     'email' => $request->email,
        //     'firstName' => $request->firstName,
        //     'lastName' => $request->lastname,
        //     "emailVerified" => false,
        //     "realmRoles" => ["akses-admin"],
        //     'clientRoles' => [
        //         'Portal-nasional' => ['asn']
        //     ],
        //     "access" => [
        //         "manageGroupMembership" => true,
        //         "view" => true,
        //         "mapRoles" => true,
        //         "impersonate" => true,
        //         "manage" => true
        //     ],
        //     'credentials' => [[
        //         'type' => 'password',
        //         'value' => $request->password,
        //         'temporary' => false
        //     ]]
        // ];

        $array = [
            'enabled' => true,
            'email' => $request->email,
            'username' => $request->username,
            'email' => $request->email,
            'firstName' => $request->firstname,
            'lastName' => $request->lastname,
            "emailVerified" => false,
            "access" => [
                "manageGroupMembership" => true,
                "view" => true,
                "mapRoles" => true,
                "impersonate" => true,
                "manage" => true
            ],
            'credentials' => [[
                'type' => 'password',
                'value' => $request->password,
                'temporary' => false
            ]],
            "requiredActions" => [],
        ];

        // return json_encode($array);

        $respone = $client->post(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users/', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->getToken(),
            ],
            'body' => json_encode($array),
        ]);

        // $response = $client->request('POST', env('KEYCLOAK_SERVER_URI') . 'auth/realms/SPBE/protocol/openid-connect/token', [
        //     'form_params' => [
        //         'grant_type' => 'password',
        //         'client_id' => env('KEYCLOAK_SERVER_ID'),
        //         'client_secret' => env('KEYCLOAK_SERVER_SECRET'),
        //         'username' => $request->username,
        //         'password' => $request->password,
        //     ]
        // ]);
        // $data = json_decode((string) $response->getBody(), true);

        // $user_data = $client->get(env('KEYCLOAK_SERVER_URI') . 'auth/realms/SPBE/protocol/openid-connect/userinfo', [
        //     'headers' => [
        //         'Accept' => 'application/json',
        //         'Authorization' => 'Bearer ' . $data['access_token'],
        //     ],
        // ]);

        // $user_data = json_decode((string) $user_data->getBody(), true);

        // $user = new User();

        // $user->username = $request->username;
        // $user->email = $request->email;
        // $user->firstName = $request->firstname;
        // $user->lastName = $request->lastname;
        // $user->password = Hash::make($request->password);
        // $user->role_id = 2;
        // $user->user_secret = 'sub';
        // // $user->user_secret = $user_data['sub'];

        // $user->save();

        // Auth::loginUsingId($user->id);
        return view('auth.success_register');
        // return redirect()->route('verify', $user->id);
    }

    public function getToken()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('KEYCLOAK_SERVER_URI') . 'auth/realms/SPBE/protocol/openid-connect/token', [
            'form_params' => [
                'client_id' => env('KEYCLOAK_SERVER_ID'),
                'client_secret' => env('KEYCLOAK_SERVER_SECRET'),
                'grant_type' => 'client_credentials'
            ]
        ]);

        $access_token = json_decode((string) $response->getBody(), true);

        return $access_token['access_token'];
    }


    public function verify($id)
    {
        $user = User::findOrFail($id);
        // dd($user);
        return view('auth.verify', compact('user'));
    }
    public function getVerify($id)
    {
        $user = User::findOrFail($id);
        $client = new \GuzzleHttp\Client();
        $response = $client->put(env('KEYCLOAK_SERVER_URI') . 'auth/admin/realms/SPBE/users/' . $user->user_secret . '/send-verify-email', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->getToken(),
            ],
        ]);

        $access_token = json_decode((string) $response->getBody(), true);

        return $access_token['access_token'];
    }
}
