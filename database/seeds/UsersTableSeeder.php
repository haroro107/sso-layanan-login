<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'username' => 'admin-testing',
                'email'    => 'admin@email.com',
                'firstname'    => 'Admin',
                'lastname'    => 'Testing',
                'password' =>  Hash::make('123'),
                'roles'    => ' ',
                'role_id'    => 1,
            ],
        ]);
    }
}
