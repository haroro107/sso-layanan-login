@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5>Data User</h5>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <table class="table">
                        <tr>
                            <td style="width: 100px">Username</td>
                            <td>: {{$user->username}}</td>
                        </tr>
                        <tr>
                            <td>First Name</td>
                            <td>: {{$user->firstname}}</td>
                        </tr>
                        <tr>
                            <td>Last Name</td>
                            <td>: {{$user->lastname}}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>: {{$user->email}}</td>
                        </tr>
                        <tr>
                            <td>Role</td>
                            <td>: {{$user->roles}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header">
                    <a href="{{env('KEYCLOAK_SERVER_URI')."auth/realms/SPBE/protocol/openid-connect/auth?client_id=".env("KEYCLOAK_SERVER_ID")."&state=&redirect_uri=".env('KEYCLOAK_SERVER_REDIRECT_URI')."&response_type=code"}}" class="btn btn-outline-success float-right">Refresh</a>
                    <h5>Aplikasi yang terkoneksi</h5>
                </div>


                <div class="card-body">
                    <table class="table">
                        @forelse ($role_mappings as $item)
                        <tr>
                            <td>
                                <a href="{{env('KEYCLOAK_SERVER_URI')."auth/realms/SPBE/protocol/openid-connect/auth?client_id=".$item->client_name."&state=&redirect_uri=".$item->redirectUris."&response_type=code"}}" class="btn btn-outline-primary">Aplikasi {{$item->client_name}}</a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td>
                                <b> Tidak ada aplikasi yang terkoneksi</b>
                            </td>
                        </tr>
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection