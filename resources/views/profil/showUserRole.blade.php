@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Data User Role</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <table class="table">
                        <tr>
                            <th>Username</th>
                            <th>Role</th>
                        </tr>
                        @foreach ($user as $item)
                        <tr>
                            <td>{{$item->username}}</td>
                            <td>
                                <a href="{{route('showUserRoleID',$item->id)}}" class="btn btn-outline-primary">
                                    Update
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection