@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="">
                        @csrf

                        <p>Data Berhasil Dibuat</p>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                {{-- <button type="submit" class="btn btn-outline-primary">
                                    {{ __('Login') }}
                                </button> --}}

                                <a href="{{env('KEYCLOAK_SERVER_URI')."auth/realms/SPBE/protocol/openid-connect/auth?client_id=".env("KEYCLOAK_SERVER_ID")."&state=&redirect_uri=".env('KEYCLOAK_SERVER_REDIRECT_URI')."&response_type=code"}}" class="btn btn-outline-success">Login SSO</a>

                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection